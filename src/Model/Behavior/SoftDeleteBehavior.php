<?php

	namespace SoftDelete\Model\Behavior;

	use Cake\ORM\Behavior;
	use Cake\Datasource\EntityInterface;
	use Cake\Event\Event;
	use Cake\ORM\Query;
	use ArrayObject;

	class SoftDeleteBehavior extends Behavior {

		protected $_config;

		public function initialize(array $config) {
		    $defaultConfig = [
					'field' => 'is_deleted',
				];
				$this->_config = array_merge($config, $defaultConfig);
		}

		public function beforeSave(Event $event, EntityInterface $entity) {
			if ($entity->isNew()) {
				$entity->{$this->_config['field']} = false;
			}
			return $entity;
		}

		public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
		{
			$query->where([$event->subject()->getAlias().'.'.$this->_config['field'] => false]);
		}

	}
