<?php

	namespace SoftDelete\Model\Entity;

	trait SoftDeleteTrait {

		public function softDelete() {
			$this->set('is_deleted', true);
		}

	}
